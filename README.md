Målet er at lave en tank, der kan skyde med elastikker - og som er styret via et "joystick".

Jeg har brugt en laptop med Mint Linux, men det kan sikkert også sættes op på en Mac eller under Windows, sidstnævnte er dog på egne ben.

Guiden her går ud fra du kører linux, men med simple ændringer vil det fint virke under osx.

## Krav:

1. 2 Lego Mindstorms
1. 2 SD kort flashet med [ev3dev](https://www.ev3dev.org/)
1. En laptop eller computer med [mqtt](https://www.ev3dev.org/docs/tutorials/sending-and-receiving-messages-with-mqtt/)
1. Netværk opsat mellem Mindstorms og computer via enten [wifi](https://www.ev3dev.org/docs/networking/) eller [Bluetooth](https://www.ev3dev.org/docs/tutorials/connecting-to-the-internet-via-bluetooth/)

## Byggevejledning

Jeg har desværre ikke nogen, men her er et par billeder af sættet - håber I selv kan være kreative.

### Joystik

- Højre motor er sat til port A
- Venstre motor er sat til port D
- Skyde knappen er sat til port 4

![billede ovenfra af joystikket](images/remote-1.jpg)
![billede skråt forfra af joystikket](images/remote-2.jpg)
![billede fra siden af joystikket](images/remote-3.jpg)

### Tank

- Højre motor er sat til port A
- Venstre motor er sat til port D
- Motor kanonen er sat til port B

![billede af tanken](images/tank-1.jpg)
![billede af tanken](images/tank-2.jpg)
![billede af tanken](images/tank-3.jpg)
![billede af tanken](images/tank-4.jpg)
![billede af tanken](images/tank-5.jpg)
