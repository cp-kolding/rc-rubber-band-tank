#!/usr/bin/env python3

import paho.mqtt.client as mqtt
from ev3dev.auto import *

lm = LargeMotor(OUTPUT_A)
rm = LargeMotor(OUTPUT_D)
sm = MediumMotor(OUTPUT_B)

shotPos = 0

def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("topic/drive")

def on_message(client, userdata, msg):
    payload = str(msg.payload.decode("utf-8"))
    motor, speed = payload.split(':')
    speed = int(speed)

    if (-100 <= speed <= 100):
        if (-5 <= speed <= 5):
            speed = 0

        if motor == 'lp':
            lm.duty_cycle_sp=(speed * -1)
        elif motor == 'rp':
            rm.duty_cycle_sp=(speed * -1)
        else:
            sm.run_to_rel_pos(position_sp=-124, speed_sp=900, stop_action="brake")


client = mqtt.Client()
client.connect("10.42.0.1",1883,60)

client.on_connect = on_connect
client.on_message = on_message

lm.run_direct()
lm.duty_cycle_sp=0

rm.run_direct()
rm.duty_cycle_sp=0

print('ready to roll...')

client.loop_forever()
