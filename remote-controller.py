#!/usr/bin/env python3

import paho.mqtt.client as mqtt
from ev3dev.auto import *
from time import sleep

leftController = LargeMotor(OUTPUT_A)
rightController = LargeMotor(OUTPUT_D)
triggerController = TouchSensor(INPUT_4)

leftController.reset()
rightController.reset()

leftPosition = leftController.position * -1
rightPosition = rightController.position * -1

client = mqtt.Client()
client.connect("10.42.0.1", 1883, 60)

print('ready to roll...')

holding = False

while True:
    lp = leftController.position * -1
    rp = rightController.position * -1

    if lp != leftPosition:
        client.publish("topic/drive", "lp:" + str(lp))
        leftPosition = lp

    if rp != rightPosition:
        client.publish("topic/drive", "rp:" + str(rp))
        rightPosition = rp

    if triggerController.value() == 1:
        if holding == False:
            client.publish("topic/drive", "shoot:1")
            holding = True
    else:
        holding = False

    sleep(0.1)

client.disconnect()
